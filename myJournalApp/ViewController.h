//
//  ViewController.h
//  myJournalApp
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۲/۸.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h" 

@interface ViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property(strong, nonatomic) NSMutableArray * numbers;
@end

