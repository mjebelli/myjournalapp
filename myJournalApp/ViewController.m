//
//  ViewController.m
//  myJournalApp
//
//  Created by Mohammad Jebelli on ۲۰۱۶/۲/۸.
//  Copyright © ۲۰۱۶ cs3260. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize myTableView, numbers;




- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"Editable Table";
    
    //add edit button
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    //add the add button
    UIBarButtonItem * addButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject)];
    
    
    self.navigationItem.rightBarButtonItem = addButton;
    
    
    
    
    NSDictionary* dictionary = numbers;
    
    
    
    numbers= [[NSMutableArray alloc]initWithObjects:@"One",@"Two",@"Three",@"Four",@"five", nil];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    
    DetailViewController* dvc = [DetailViewController new];
    
    
    [self.navigationController pushViewController:dvc animated:YES];
}





-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    
    [super setEditing:editing animated:animated];
    
    [myTableView setEditing:editing animated:animated];
    
    
    
}



-(void)insertNewObject
{
    //display alert
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Insert an enrty" message:@"" delegate:self cancelButtonTitle:@"Abort" otherButtonTitles:@"OK", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert show];
    
}


#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return numbers.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.text =  [numbers objectAtIndex:indexPath.row];
    return cell;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return YES;
    
}


-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //remove from mutable array
        [numbers removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
    
}



#pragma mark - UITableView Delegate methods

#pragma mark - UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //In case of ok button
    if (buttonIndex == 1) {
        NSString * tmpTextField = [alertView textFieldAtIndex:0].text;
    
        
        if (!numbers)
        {
            numbers = [[NSMutableArray alloc]init];
        }
        
        
        [numbers insertObjects:tmpTextField atIndexes:0];
        
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        
        [self.myTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
    }
    
    
}







@end
